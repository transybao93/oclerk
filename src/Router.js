import React from 'react';
import {
  Image,
  Text,
} from 'react-native';

import { 
  createBottomTabNavigator,
  createStackNavigator
} from 'react-navigation';

//- Stack page
import Login from './containers/Login';
import RecoverPass from './containers/RecoverPass'; 
import AskPermission from './containers/AskPermission'

//- TabNavigator page
import Statistic from './containers/Statistic';
import Home from './containers/Home';
import Profile from './containers/Profile';

//- Other screen
import CameraView from './components/common/camera';
import CameraPreview from './components/common/cameraPreview';
import ViewAllSchedule from './components/common/viewAllSchedule';
import EditProfile from './containers/EditProfile';

//- import color
import {
  activeColor,
  inactiveColor
} from './utils/color';

//- customizable icon
import { createIconSetFromIcoMoon } from 'react-native-vector-icons';
import icoMoonConfig from './utils/icons/selection.json';
const CustomIcon = createIconSetFromIcoMoon(icoMoonConfig);

const StackNavigator = createStackNavigator({
  Login: {
    screen: Login,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  RecoverPass: {
    screen: RecoverPass,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  AskPermission: {
    screen: AskPermission,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  Camera: {
    screen: CameraView,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  CameraPreview: {
    screen: CameraPreview,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  EditProfile: {
    screen: EditProfile,
    navigationOptions: ({ navigation }) => ({
      header: null,
    }),
  },
  //- Tabbar Navigation
  Main: {
    screen: createBottomTabNavigator({
      Home: {
        screen: Home,
        navigationOptions: ({ navigation }) => ({
          title: 'Home',
          tabBarIcon: ({tintColor}) => 
          // <Image
          //   source={require('../src/assets/images/avatar.jpg')}
          //   // resizeMode = {Image.resizeMode.contain}
          //   style={{ height: 30, width: 30, tintColor: 'black'}}
          //  /> 
            <CustomIcon
              name='icon-new-home-active'
              size={22}
              color={tintColor}
            />

        }),
      },
      Schedule: {
        screen: ViewAllSchedule,
        navigationOptions: ({ navigation }) => ({
          title: 'Schedule',
          tabBarIcon: ({tintColor}) => 
          // <Image
          //   source={require('../src/assets/images/avatar.jpg')}
          //   // resizeMode = {Image.resizeMode.contain}
          //   style={{ height: 30, width: 30, tintColor: 'black'}}
          //  /> 
            <CustomIcon
              name='icon-new-schedule-active'
              size={22}
              color={tintColor}
            />

        }),
      },
      Statistic: {
        screen: Statistic,
        navigationOptions: ({ navigation }) => ({
          title: 'Statistic',
          tabBarIcon: ({tintColor}) => 
            <CustomIcon
              name='icon-new-statistic-active'
              size={22}
              color={tintColor}
            />
        }),
      },
      Profile: {
        screen: Profile,
        navigationOptions: ({ navigation }) => ({
          title: 'Profile',
          tabBarIcon: ({tintColor}) => 
            <CustomIcon
              name='icon-new-profile-active'
              size={22}
              color={tintColor}
            />
        }),
      },

    },
    {
      tabBarOptions: {
        activeTintColor: activeColor,
        inactiveTintColor: inactiveColor,
      },
    }
    ),
    navigationOptions: ({ navigation }) => ({
      title: 'Home',
      order: ['Home', 'Statistic', 'Profile'],
      header: null
    }),
  },

},
{
  initialRouteName: 'Main'
}

);

export default StackNavigator;
