import React, { Component } from 'react'
import { 
  View,
  StyleSheet,
  Image,
} from 'react-native';
import Input from '../components/Login/input'

//- color
import {
  bgColor
} from '../utils/color';

export class Login extends Component {

  _onPress = () => {
    const { navigate } = this.props.navigation;
    navigate('RecoverPass');
  }

  render() {
    return (
      <View style={styles.container}>

        {/* <Text> This is login page </Text>

        <Input/>
        <TouchableOpacity onPress={this._onPress}>
          <Text> Go to recover pass page </Text>
        </TouchableOpacity> */}

        <View style={styles.logo}>
          <Image source={require('../assets/images/logo.png')} />
        </View>

        <View style={styles.input}>
          <Input/>
        </View>


      </View>
    )
  }
}

export default Login

const styles = StyleSheet.create({

  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: bgColor
  },

  logo: {
    flex: 2,
    alignContent: 'center',
    alignItems: 'center',
    justifyContent: 'center'
  },
  input: {
    flex: 3.5,
    justifyContent: 'center',
    alignItems: 'center',
  },

})