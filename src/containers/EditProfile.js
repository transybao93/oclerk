import React, { Component } from 'react'
import { 
    Text, 
    View,
    StyleSheet,
    TextInput
} from 'react-native'
import _ from 'lodash';
import BasicHeader from '../components/common/basicHeader';

//- import color
import {
    bgColor
} from '../utils/color';
  
//- margin
import {
    left,
    right
} from '../utils/margin';

export class EditProfile extends Component {

    constructor(props) {
        super(props)
        this.state = { 
            params: this.props.navigation.state.params,
            text: this.props.navigation.state.params.text,
            id: this.props.navigation.state.params.userID,
            inputName: this.props.navigation.state.params.type,
            oldPasswordPlaceholder: 'Your password',
            newPasswordPlanceholder: 'Your new password',
            confirmPasswordPlaceholder: 'Confirm your password'
        };
    }

    _onButtonPress = () => {

        //- check if text or password
        if(_.size(this.state.text) > 0)
        {
            console.log('not password edit')
            this.setState({
                text: ''
            })
        }else{
            console.log('password edit')
            this.setState({
                oldPasswordPlaceholder: '',
                newPasswordPlanceholder: '',
                confirmPasswordPlaceholder: ''
            })
        }
    }

    render() {
        return (
        <View style={styles.container}>
            <BasicHeader
                headerTitle={'Edit profile'}
                navigation={this.props.navigation}
                haveRightButton={true}
                rightButtonText={'Save'}
                rightButtonOnPress={this._onButtonPress}
            />
            

            {(this.state.inputName !== "Password") &&
                <View style={styles.content}>
                    <TextInput
                        style={{height: 40, borderColor: 'gray', borderBottomWidth:0.5}}
                        onChangeText={(text) => this.setState({text})}
                        value={this.state.text}
                        keyboardType={(this.state.inputName === "Phone number") ? 'phone-pad' : 'default'}
                        returnKeyType={'done'}
                        enablesReturnKeyAutomatically={true}
                        clearButtonMode={'unless-editing'}
                    />
                </View>
            }

            {(this.state.inputName === "Password") &&
                <View style={styles.content}>
                    <TextInput
                        style={{height: 40, borderColor: 'gray', borderBottomWidth:0.5}}
                        onChangeText={(oldPasswordPlaceholder) => this.setState({oldPasswordPlaceholder})}
                        // value={this.state.oldPasswordPlaceholder}
                        placeholder={this.state.oldPasswordPlaceholder}
                        returnKeyType={'next'}
                        //- go to next text input
                        onSubmitEditing={() => { this.newPass.focus(); }}
                        //- secure
                        secureTextEntry={true}
                        enablesReturnKeyAutomatically={true}
                    />
                    <TextInput
                        style={{height: 40, borderColor: 'gray', borderBottomWidth:0.5}}
                        onChangeText={(newPasswordPlanceholder) => this.setState({newPasswordPlanceholder})}
                        // value={this.state.newPasswordPlanceholder}
                        placeholder={this.state.newPasswordPlanceholder}
                        returnKeyType={'next'}
                        //- when click next
                        onSubmitEditing={() => { this.confirmPass.focus(); }}
                        ref={(input) => { this.newPass = input; }}
                        //- secure
                        secureTextEntry={true}
                        enablesReturnKeyAutomatically={true}
                    />
                    <TextInput
                        style={{height: 40, borderColor: 'gray', borderBottomWidth:0.5}}
                        onChangeText={(confirmPasswordPlaceholder) => this.setState({confirmPasswordPlaceholder})}
                        // value={this.state.confirmPasswordPlaceholder}
                        placeholder={this.state.confirmPasswordPlaceholder}
                        //- when done
                        returnKeyType={'done'}
                        ref={(input) => { this.confirmPass = input; }}
                        //- secure
                        secureTextEntry={true}
                        enablesReturnKeyAutomatically={true}
                    />
                </View>
            }

            
        </View>
        )
    }
}

export default EditProfile

const styles = StyleSheet.create({

    container: {
        flex: 1,
        paddingLeft: left,
        paddingRight: right,
        backgroundColor: bgColor
    },
    content: {
        flex: 4
    }

});