import React, { Component } from 'react'
import { 
  Text, 
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  StatusBar
} from 'react-native'
import { 
  MAIN_COLOR
} from "../helper/constant";
import Header from '../components/common/header';
import TabList from '../components/Home/TabList';

//- import color
import {
  bgColor
} from '../utils/color';

//- margin
import {
  left,
  right
} from '../utils/margin';

//- schedule
import Schedule from '../components/common/schedule';

export class Home extends Component {

  componentDidMount() {
    StatusBar.setHidden(true);
  }

  render() {
    return (
      <ScrollView 
        style={styles.container}
        showsVerticalScrollIndicator={true}
      >
        {/* header */}
        <Header
          isDisplayWelcomeText={true}
          isDisplayDateTimeText={true}
          navigation={this.props.navigation}
          isMarginTop={true}
          marginTop={25}
        />

        <Header
          title={'Attendance'}
          isDisplayWelcomeText={false}
          isDisplayDateTimeText={false}
          titleFontSize={24}
          isMarginTop={true}
          marginTop={21}
          haveBottomLine={true}
          isShowAvatar={false}
        />
        
        {/* tab lisTabt */}
        <TabList
          navigation={this.props.navigation}
        />

        {/* schedule 
        <Schedule
          navigation={this.props.navigation}
        />*/}

      </ScrollView>
    )
  }
}

export default Home

const styles = StyleSheet.create({

  container: {
    flex: 1,
    paddingLeft: left,
    paddingRight: right,
    backgroundColor: bgColor,
  }

})