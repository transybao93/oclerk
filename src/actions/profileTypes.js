
export const UPDATE_PROFILE = "UPDATE_PROFILE";
export const CHECK_IN = "CHECK_IN";
export const CHECK_OUT = "CHECK_OUT";
export const VIEW_SCHEDULE = "VIEW_SCHEDULE";
export const VIEW_STATISTIC = "VIEW_STATISTIC";