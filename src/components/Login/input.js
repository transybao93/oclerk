import React, { Component } from 'react'
import { 
    Text,
    View,
    StyleSheet,
    TouchableOpacity,
    Image
} from 'react-native'
import { 
    FormInput,
    Button
} from 'react-native-elements'
import { 
    BUTTON_MAIN_COLOR,
    LEFT_RIGHT_MARGIN
} from "../../helper/constant";

//- import font
import {
    body1
} from '../../utils/fonts';

export class input extends Component {

    constructor(props){
        super(props)
        this.state = {
            text: "hehe"
        }
    }

    render() {
        return (
            <View style={styles.container}>

                <FormInput 
                    placeholder={'Mobile number'}
                    containerStyle={styles.containerStyle}
                    inputStyle={styles.inputStyle}
                />
                <FormInput 
                    placeholder={'Password'}
                    containerStyle={styles.containerStyle}
                    inputStyle={styles.inputStyle}
                />
                <Button 
                    title="Sign in"
                    borderRadius={6}
                    backgroundColor={BUTTON_MAIN_COLOR}
                    buttonStyle={styles.buttonStyle}
                    textStyle={styles.textStyle}
                />
                <TouchableOpacity style={styles.recoverPass}>
                    <Text style={styles.recoverPassText}>
                        Recover password?
                    </Text>
                </TouchableOpacity>
            </View>
        )
    }
}

export default input

const styles = StyleSheet.create({

    container:{
        flex: 1,
        width: '100%',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        marginLeft: LEFT_RIGHT_MARGIN,
        marginRight: LEFT_RIGHT_MARGIN,
    },

    containerStyle: {
        borderWidth: 0.25,
        marginTop: 10,
        borderRadius: 6,
        borderColor: '#00224B',
    },

    buttonStyle:{
        marginTop: 10,
    },

    inputStyle: {
        fontSize: 14,
        paddingLeft: 10
    },

    textStyle:{
        fontSize: 16
    },

    recoverPass: {
        marginTop: 6,
        alignItems: 'center',
    },
    recoverPassText: {
        fontFamily: body1,
        fontSize: 14
    },
})