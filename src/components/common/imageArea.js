import React, { Component } from 'react'
import { 
    Text, 
    View,
    StyleSheet,
    Image
} from 'react-native'

//- color
import {
    imageAreaColor
} from '../../utils/color';

//- font
import {
    h6Font
} from '../../utils/fonts';

import PropTypes from 'prop-types';
import moment from 'moment';

export class ImageArea extends Component {
  render() {
    const { username } = this.props;
    return (
        <View style={styles.container}>
            <Image style={styles.image} source={require('../../assets/images/avatar.jpg')}/>
            <Text style={styles.username}>{username}</Text>
        </View>
    )
  }
}

export default ImageArea

const styles = StyleSheet.create({

    container: {
        flex: 1,
        backgroundColor: imageAreaColor,
    },

    image: {
        width: 120,
        height: 120,
        borderRadius: 120/2,
        overflow: 'hidden',
        alignSelf: 'center',
        marginBottom: 16,
        resizeMode: 'contain',
        marginTop: 15,
    },

    username: {
        fontFamily: h6Font,
        fontSize: 18,
        alignSelf: 'center',
        paddingBottom: 10,
    },

});

//- prop types
ImageArea.propTypes = {
    username: PropTypes.string
}

//- default
ImageArea.defaultProps = {
    username: "John William"
}
  