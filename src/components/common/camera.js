import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    Dimensions,
    Button, 
    TouchableOpacity,
    StatusBar,
    Image,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import moment from 'moment';

import Ionicons from 'react-native-vector-icons/Ionicons';

//- margin
import {
    left,
    right
  } from '../../utils/margin';

class CameraView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            imageLink: "",
            navigation: this.props.navigation,
            cameraType: RNCamera.Constants.Type.front,
            initialPosition: 'unknown',
            currentDateTime: '',
            params: this.props.navigation.state.params,
            type: this.props.navigation.state.params.type,

        };
        console.log('navigation on camera', this.props.navigation)
        console.log('type', this.state.type)
    }

    _goBack = () => {
        this.props.navigation.pop();
    }

    componentDidMount() {
        StatusBar.setHidden(true);
    }

    _changeCameraType = () => {
        if (this.state.cameraType === RNCamera.Constants.Type.front) {
            this.setState({
              cameraType: RNCamera.Constants.Type.back,
            });
          } else {
            this.setState({
              cameraType: RNCamera.Constants.Type.front,
            });
          }
    }

    takePicture = async function() {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options)
            console.log(data.uri);

            //- get current location
            await navigator.geolocation.getCurrentPosition(
                (position) => {
                const initialPosition = JSON.stringify(position);
                console.log('initial position...', initialPosition.coords);
                this.setState({ initialPosition });
                },
                (error) => console.log('error',error.message),
                { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
            );
            
            this.setState({
                imageLink: data.uri,
                currentDateTime: moment().format('hh:mm:ss MMMM Do, YYYY')
            })  
        }
    };

    _getCurrentLocation = () => {
        console.log('geolocation', navigator.geolocation);
        navigator.geolocation.getCurrentPosition(
            (position) => {
               const initialPosition = JSON.stringify(position);
               console.log('initial position...', initialPosition.coords.longitude);
               this.setState({ initialPosition });
            },
            (error) => console.log('error',error.message),
            { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 }
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.top}>
                    
                    {/* with flash */}
                    <TouchableOpacity onPress={this._goBack} style={{width: 50}}>
                        <Image
                            source={require('../../assets/images/cameraIcon/back.png')}
                        />
                    </TouchableOpacity>

                </View>
                <RNCamera
                    ref={ref => {
                    this.camera = ref;
                    }}
                    style = {styles.preview}
                    type={this.state.cameraType}
                    flashMode={RNCamera.Constants.FlashMode.off}
                    permissionDialogTitle={'Permission to use camera'}
                    permissionDialogMessage={'We need your permission to use your camera phone'}
                    onGoogleVisionBarcodesDetected={({ barcodes }) => {
                        console.log(barcodes)
                    }}
                />

                <View style={styles.bottom}>
                    
                    {/* reverse camera */}
                    <View style={styles.rotateCamera}>
                        <TouchableOpacity onPress={this._changeCameraType} style={{width: 50}}>
                            <Image
                                source={require('../../assets/images/cameraIcon/cameraRotate.png')}
                            />
                        </TouchableOpacity>
                    </View>

                    <TouchableOpacity onPress={this.takePicture.bind(this)}>
                        <Image
                            source={require('../../assets/images/cameraIcon/home.png')}
                        />
                    </TouchableOpacity>

                    {/* Image preview */}
                    
                        <View style={styles.imagePreview}>
                            { (this.state.imageLink !== "") &&
                                <TouchableOpacity style={styles.image} onPress={() => this.props.navigation.navigate('CameraPreview', {
                                    imageURI: this.state.imageLink,
                                    currentDateTime: this.state.currentDateTime,
                                    currentLocation: this.state.initialPosition,
                                    type: this.state.type
                                })
                            }>
                                    <Image
                                        style={{width: '100%', height: '100%'}}
                                        source={{uri: this.state.imageLink}}
                                    />
                                </TouchableOpacity>
                            }
                        </View>
                    

                </View>

            </View>
        );
    }
}

export default CameraView;


const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },

    // top bar 
    top: {
        flex: 0.1,
        backgroundColor: 'black',
        alignContent: 'center',
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingLeft: left
    },
    
    //- bottom bar
    bottom: {
        flex: 0.2,
        flexDirection: 'row',
        justifyContent: 'center',
        backgroundColor: 'black',
        borderTopWidth: 1,
        justifyContent: 'space-between',
        alignItems: 'center',
        alignContent: 'center',
    },
    rotateCamera: {
        alignContent: 'center',
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: left
    },
    capture: {
        flex: 0,
        width: 50,
        height: 50,
        backgroundColor: '#fff',
        borderRadius: 50/2,
    },
    image: { 
        // borderWidth: 1, 
        // borderColor: 'white',
        borderRadius: 6,
    },
    imagePreview: {
        marginRight: right,
        width: 50, 
        height: 50,
    },

});