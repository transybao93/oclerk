import React, { Component } from 'react'
import { 
    Text, 
    View,
    StyleSheet 
} from 'react-native'

import PropTypes from 'prop-types';
import moment from 'moment';

//- import singleTab component
import SingleTab from '../common/singleTab';

export class TabList extends Component {
  render() {
    return (
      <View style={styles.container}>

        {/* Row 1 */}
        <View style={styles.row1}>
            <SingleTab
                tabName={'Check in'}
                imageLink={'checkIn'}
                navigation={this.props.navigation}
                goTo={'Camera'}
            />
            <SingleTab 
                isMarginLeft={true}
                left={19}
                tabName={'Check out'}
                imageLink={'checkOut'}
                navigation={this.props.navigation}
                goTo={'Camera'}
            />
        </View>

        {/* Row 2 */}
        <View style={styles.row2}>
            <SingleTab
                tabName={'Start lunch'}
                imageLink={'startLunch'}
            />
            <SingleTab 
                isMarginLeft={true}
                left={19}
                tabName={'Finish lunch'}
                imageLink={'finishLunch'}
            />
        </View>
      </View>
    )
  }
}

export default TabList

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
    },
    row1: {
        flex: 1,
        flexDirection: 'row'
    },
    row2: {
        flex: 1,
        flexDirection: 'row'
    },
    left: {
        marginLeft: 19,
    }

});