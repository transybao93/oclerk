import { combineReducers } from 'redux';
import BaseNavigation from '../Router';
//- child reducer
import profile from './profile';

export default combineReducers({
    navigation: (state, action) => BaseNavigation.router.getStateForAction(action, state),
    state: (state = {}) => state,
    profile
});
