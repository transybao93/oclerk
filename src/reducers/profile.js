const profile = (state = [], action) => {
    switch (action.type) {
        case 'UPDATE_PROFILE':
            return [
                // ...state,
                {
                    id: action.id,
                    text: action.text,
                    completed: false
                }
            ]
        default:
            return state
    };
}

export default profile